# ePaper Calendar

![Photo of epapercalendar](./photo.png?raw=true "photo of epapercalendar")


Minimalist desk calendar using epaper/eink, emulating the look of a tear-away calendar

