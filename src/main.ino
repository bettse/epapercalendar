#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecure.h>

#include <ezTime.h>
#include <ArduinoJson.h>

#define ENABLE_GxEPD2_GFX 0
#include <GxEPD2_3C.h>
#include <GxEPD2_BW.h>

#include <Fonts/FreeSans12pt7b.h>
#include <Fonts/FreeSansBold24pt7b.h>
#include <Fonts/FreeSans64pt7b.h>

#include "waveshare-demo-epd42.h" // combined e-Paper driver and 4.2" display code

// ***** for mapping of Waveshare e-Paper ESP8266 Driver Board *****
GxEPD2_3C<GxEPD2_420c, GxEPD2_420c::HEIGHT / 2> display(GxEPD2_420c(/*CS=15*/ SS, /*DC=4*/ 4, /*RST=5*/ 5, /*BUSY=16*/ 16));

ESP8266WiFiMulti WiFiMulti;
String loading = "Loading...";
uint16_t third;

void setup() {
  Serial.begin(115200);
  // Serial.setDebugOutput(true);

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("407", "ericbetts");

  // e-paper display SPI initialization has to come before SPIFFS or it'll reset
  pinMode(CS_PIN, OUTPUT);
  pinMode(RST_PIN, OUTPUT);
  pinMode(DC_PIN, OUTPUT);
  pinMode(BUSY_PIN, INPUT);
  SPI.begin();

  display.setRotation(1);
  // Calculate after rotation because it might flip width/height
  third = display.height()/3;
}

int16_t tbx, tby;
uint16_t tbw, tbh;
String current_month, current_day;
bool synced = false;
Timezone myTZ;
long previousMillis = 0;
long interval = 1000 * 60 * 60;

void loop() {
  unsigned long currentMillis = millis();
  //TODO: If the device is booted and can't get on wifi
  // or can't get time for some amount of time, display error

  // wait for WiFi connection
  if ((WiFiMulti.run() == WL_CONNECTED)) {
    if (!synced) {
      synced = waitForSync(1000);
  	  myTZ.setLocation(F("America/Los_Angeles"));
      Serial.println("[TIME] synced");
    }

    if(previousMillis == 0 || currentMillis - previousMillis > interval) {
      previousMillis = currentMillis;

      String month = myTZ.dateTime("F");
      String day = myTZ.dateTime("d");
      Serial.println("[TIME] current date (" + month + "," + day + ")");

      if (current_month != month || current_day != day) {
        newDay();
      } // end month/day change
    } // end interval
  } // end wifi connected
  // For ezTime
  events();
}


void newDay() {
  String month = myTZ.dateTime("F");
  String day = myTZ.dateTime("d");

  Serial.println("[TIME] date changed (" + month + "," + day + ")");
  current_month = month;
  current_day = day;

  Serial.println("[EPD] init");
  display.init();
  display.setFullWindow();

  uint16_t month_x, month_y, day_x, day_y;
  uint16_t holiday_x, holiday_y;
  String holiday = getHoliday();

  display.firstPage();
  do {
    display.fillRect(0, 0, display.width(), third, GxEPD_RED);

    display.setTextColor(GxEPD_WHITE);
    display.setFont(&FreeSansBold24pt7b);
    display.getTextBounds(month, 0, 0, &tbx, &tby, &tbw, &tbh);
    month_x = (display.width() - tbw) / 2;
    month_y = (third + tbh) / 2;
    display.setCursor(month_x, month_y);
    display.println(month);

    display.setTextColor(GxEPD_BLACK);
    display.setFont(&FreeSans64pt7b);
    display.getTextBounds(day, 0, 0, &tbx, &tby, &tbw, &tbh);
    day_x = (display.width() - tbw) / 2;
    day_y = ((2*third + tbh) / 2) + third;
    display.setCursor(day_x, day_y);
    display.println(day);

    //Holiday
    if (holiday.length() > 0) {
      display.setFont(&FreeSans12pt7b);
      display.getTextBounds(holiday, 0, 0, &tbx, &tby, &tbw, &tbh);
      holiday_x = (display.width() - tbw) / 2;
      holiday_y = (display.height() - tbh);
      display.setCursor(holiday_x, holiday_y);
      display.println(holiday);
    }

  } while (display.nextPage());
  display.powerOff();
  Serial.println("[EPD] off");
}

const char fingerprint[] PROGMEM = "FA 75 F4 C6 9E 2F 5A 82 63 02 00 5B 9B F0 FE 13 E8 93 55 ED";

String getHoliday () {
  String response = getHolidayApi();
  if (response.length() > 2) {
    StaticJsonDocument<256> doc;
    DeserializationError error = deserializeJson(doc, response);
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.c_str());
      return "";
    }

    int status = doc["status"]; // 200
    if (status == 200) {
      JsonArray holidays = doc["holidays"];
      if (holidays.size() > 0) {
        // Just use the first holiday when there is more than one
        JsonObject holidays_0 = doc["holidays"][0];
        const char* holidays_0_name = holidays_0["name"]; // "Christmas"
        return holidays_0_name;
      }
    }
  }
  return "";
}

String getHolidayApi() {
  // Although they're strings, there are numeric representations of the fields
  String month = myTZ.dateTime("n");
  String day = myTZ.dateTime("j");
  String year = myTZ.dateTime("Y");
  String url = "https://holidayapi.pl/v1/holidays?country=US&year=" + year + "&month=" + month + "&day=" + day;

	HTTPClient http;
  WiFiClientSecure client;
  client.setFingerprint(fingerprint);

  Serial.println("[HTTP] begin...");
  if (http.begin(client, url)) {

    Serial.println("[HTTP] GET... " + url);
    // start connection and send HTTP header
    int httpCode = http.GET();

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        String payload = http.getString();
        Serial.println(payload);
        return payload;
      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
  } else {
    Serial.printf("[HTTP} Unable to connect\n");
  }
  return "";
}

